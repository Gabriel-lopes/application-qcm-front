package com.example.Interfaces;
import java.util.ArrayList;
import com.example.models.Qcm;


public interface IRequestQcm {
    public void receiveQcms(ArrayList<Qcm> qcms);
    public void receiveQcm(Qcm qcm);
}
