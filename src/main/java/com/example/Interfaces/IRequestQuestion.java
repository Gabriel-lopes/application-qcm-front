package com.example.Interfaces;

import java.util.ArrayList;

import com.example.models.Question;

public interface IRequestQuestion {
    public void receiveQuestions(ArrayList<Question> questions);
    public void receiveQuestion(Question question);
}
