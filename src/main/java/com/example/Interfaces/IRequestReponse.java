package com.example.Interfaces;

import java.util.ArrayList;

import com.example.models.Reponse;

public interface IRequestReponse {
    public void receiveReponses(ArrayList<Reponse> reponses);
    public void receiveReponse(Reponse reponse);
}
