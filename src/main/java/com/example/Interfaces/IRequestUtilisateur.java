package com.example.Interfaces;

import java.util.ArrayList;

import com.example.models.Utilisateur;

public interface IRequestUtilisateur {
    public void receiveReponses(ArrayList<Utilisateur> utilisateurs);
    public void receiveReponse(Utilisateur utilisateur);
}
