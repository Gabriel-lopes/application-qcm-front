package com.example.models;

import java.util.Objects;

public class Question {
    private int id, idqcm;
    private String question;


    public Question() {
    }

    public Question(int id, int idqcm, String question) {
        this.id = id;
        this.idqcm = idqcm;
        this.question = question;
    }
    public Question(int idqcm, String question) {
        this.idqcm = idqcm;
        this.question = question;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdqcm() {
        return this.idqcm;
    }

    public void setIdqcm(int idqcm) {
        this.idqcm = idqcm;
    }

    public String getQuestion() {
        return this.question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Question id(int id) {
        setId(id);
        return this;
    }

    public Question idqcm(int idqcm) {
        setIdqcm(idqcm);
        return this;
    }

    public Question question(String question) {
        setQuestion(question);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Question)) {
            return false;
        }
        Question question = (Question) o;
        return id == question.id && idqcm == question.idqcm && Objects.equals(question, question.question);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idqcm, question);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", idqcm='" + getIdqcm() + "'" +
            ", question='" + getQuestion() + "'" +
            "}";
    }

}
