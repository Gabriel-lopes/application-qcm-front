package com.example.models;

import java.util.Objects;

public class Reponse {
    private int id;
    private String reponse;
    private Boolean veracite;
    private Integer question;


    public Reponse() {
    }

    public Reponse(int id, String reponse, Boolean veracite, Integer question) {
        this.id = id;
        this.reponse = reponse;
        this.veracite = veracite;
        this.question = question;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReponse() {
        return this.reponse;
    }

    public void setReponse(String reponse) {
        this.reponse = reponse;
    }

    public Boolean isVeracite() {
        return this.veracite;
    }

    public Boolean getVeracite() {
        return this.veracite;
    }

    public void setVeracite(Boolean veracite) {
        this.veracite = veracite;
    }

    public Integer getQuestion() {
        return this.question;
    }

    public void setQuestion(Integer question) {
        this.question = question;
    }

    public Reponse id(int id) {
        setId(id);
        return this;
    }

    public Reponse reponse(String reponse) {
        setReponse(reponse);
        return this;
    }

    public Reponse veracite(Boolean veracite) {
        setVeracite(veracite);
        return this;
    }

    public Reponse question(Integer question) {
        setQuestion(question);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Reponse)) {
            return false;
        }
        Reponse reponse = (Reponse) o;
        return id == reponse.id && Objects.equals(reponse, reponse.reponse) && Objects.equals(veracite, reponse.veracite) && Objects.equals(question, reponse.question);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, reponse, veracite, question);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", reponse='" + getReponse() + "'" +
            ", veracite='" + isVeracite() + "'" +
            ", question='" + getQuestion() + "'" +
            "}";
    }

}
