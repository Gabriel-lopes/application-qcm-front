package com.example.models;

import java.util.Objects;

public class Utilisateur {
    private int id;
    private String nom, prenom, email;

    public Utilisateur() {
    }

    public Utilisateur(int id, String nom, String prenom, String email) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Utilisateur id(int id) {
        setId(id);
        return this;
    }

    public Utilisateur nom(String nom) {
        setNom(nom);
        return this;
    }

    public Utilisateur prenom(String prenom) {
        setPrenom(prenom);
        return this;
    }

    public Utilisateur email(String email) {
        setEmail(email);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Utilisateur)) {
            return false;
        }
        Utilisateur utilisateur = (Utilisateur) o;
        return id == utilisateur.id && Objects.equals(nom, utilisateur.nom) && Objects.equals(prenom, utilisateur.prenom) && Objects.equals(email, utilisateur.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom, email);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", email='" + getEmail() + "'" +
            "}";
    }

}
