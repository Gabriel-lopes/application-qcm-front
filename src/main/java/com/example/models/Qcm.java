package com.example.models;

import java.util.Objects;

public class Qcm {
    private int id;
    private String nom;


    public Qcm() {
    }

    public Qcm(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }
    public Qcm(String nom) {
        this.nom = nom;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Qcm id(int id) {
        setId(id);
        return this;
    }

    public Qcm nom(String nom) {
        setNom(nom);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Qcm)) {
            return false;
        }
        Qcm qcm = (Qcm) o;
        return id == qcm.id && Objects.equals(nom, qcm.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", nom='" + getNom() + "'" +
            "}";
    }

}
