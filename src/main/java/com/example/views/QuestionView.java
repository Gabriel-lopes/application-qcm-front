package com.example.views;

import javax.swing.JPanel;

import com.example.models.Question;
import com.example.models.Reponse;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class QuestionView extends JPanel implements ActionListener{
    private Question question;
    private JLabel label;
    private JButton button_question;
    private JPanel unPanel = new JPanel();
	private JButton btDeconnexion = new JButton("Déconnexion");


    public QuestionView(Question q) {
        super();
        setLayout(new GridBagLayout());
        question = q;
		label= new JLabel(q.getQuestion() + "  ");
        button_question= new JButton("Voir les réponses");

		label.setAlignmentX(CENTER_ALIGNMENT);
		button_question.setAlignmentX(CENTER_ALIGNMENT);
        button_question.setSize(30, 20);
        button_question.addActionListener(this);
		add(label);
        add(button_question);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.button_question)
        {
            ReponseViewApp.getInstance(question.getId());
        }
        
        
    } 
    
}
