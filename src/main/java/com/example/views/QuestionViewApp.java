package com.example.views;

import java.util.ArrayList;

import javax.swing.JFrame;

import com.example.models.Question;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.*;

import com.example.Interfaces.IRequestQcm;
import com.example.Interfaces.IRequestQuestion;
import com.example.Services.ServiceQuestionAPI;

public class QuestionViewApp extends JFrame implements IRequestQuestion, ActionListener{
    GridBagConstraints constraints;
    JPanel panelContacts;
    JButton seDeconnecterBt;
    private int qcmId;

    private static QuestionViewApp instance;

    public static QuestionViewApp getInstance(int qcmId){
        if(instance==null){
            instance= new QuestionViewApp(qcmId);
        }
        return instance;
    }

    private QuestionViewApp(int qcmId){
        super("Questions");
        this.qcmId= qcmId;
        setVisible(true);
        setSize(600, 400);
        setLayout(new GridBagLayout());
        panelContacts= new JPanel();
        panelContacts.setLayout(new GridBagLayout());
        // Crée un objet de contraintes
        constraints= new GridBagConstraints();
        constraints.anchor = GridBagConstraints.NORTH;
        constraints.gridx = 0; 
        constraints.gridy = 0; 
        constraints.weighty=1;
        add(panelContacts, constraints); 
        constraints.anchor = GridBagConstraints.EAST;
        constraints.gridy = 1;
        constraints.weighty=1;
        seDeconnecterBt = new JButton("Se déconnecter");
        seDeconnecterBt.setAlignmentX(CENTER_ALIGNMENT);
        seDeconnecterBt.addActionListener(this);
        add(seDeconnecterBt, constraints);
        try {
            ServiceQuestionAPI.getQuestions(this, qcmId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }

    @Override
    public void receiveQuestions(ArrayList<Question> questions) {
        constraints.gridx = 0; 
        constraints.gridy = 0;
        panelContacts.removeAll();
        constraints.anchor = GridBagConstraints.NORTH;
        for(Question c : questions){
            QuestionView questionView= new QuestionView(c);
            panelContacts.add(questionView, constraints);
            constraints.gridy++; 
        }
        validate();
        repaint();
        
    }

    @Override
    public void receiveQuestion(Question question) {
        try {
            ServiceQuestionAPI.getQuestions(this, qcmId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.seDeconnecterBt)
        {
            dispose();
        }
    }
}
