package com.example.views;
import javax.swing.JFrame;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.*;
import com.example.Interfaces.IRequestReponse;
import com.example.Services.ServiceReponseAPI;
import com.example.models.Reponse;

public class ReponseViewApp extends JFrame implements IRequestReponse, ActionListener{

    GridBagConstraints constraints;
    JPanel panelContacts;
    JButton seDeconnecterBt;
    JButton envoie_reponse;
    private int idQuestion;
    private static ReponseViewApp instance;

    public static ReponseViewApp getInstance(int idQuestion){
        if(instance==null){
            instance= new ReponseViewApp(idQuestion);
        }
        return instance;
    }

    private ReponseViewApp(int idQuestion){
        super("Réponses");
        this.idQuestion = idQuestion;
        setVisible(true);
        setSize(600, 400);
        setLayout(new GridBagLayout());
        panelContacts= new JPanel();
        panelContacts.setLayout(new GridBagLayout());
        // Crée un objet de contraintes
        constraints= new GridBagConstraints();
        constraints.anchor = GridBagConstraints.NORTH;
        constraints.gridx = 0; 
        constraints.gridy = 0; 
        constraints.weighty=1;
        add(panelContacts, constraints); 
        constraints.anchor = GridBagConstraints.EAST;
        constraints.gridy = 1;
        constraints.weighty=1;
        seDeconnecterBt = new JButton("Se déconnecter");
        seDeconnecterBt.setAlignmentX(CENTER_ALIGNMENT);
        seDeconnecterBt.addActionListener(this);

        envoie_reponse = new JButton("Envoyer votre réponse");
        envoie_reponse.setAlignmentX(CENTER_ALIGNMENT);
        envoie_reponse.addActionListener(this);

        add(seDeconnecterBt, constraints);
        add(envoie_reponse);
        try {
            ServiceReponseAPI.getReponses(this, idQuestion);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.seDeconnecterBt)
        {
            dispose();
        }
        if(e.getSource() == this.envoie_reponse)
        {
            JOptionPane.showMessageDialog(this, "Votre réponse a été énvoyée, nous vous invitons à consulter le score");
            dispose();
        }
        
    }

    @Override
    public void receiveReponses(ArrayList<Reponse> reponses) {
        constraints.gridx = 0; 
        constraints.gridy = 0;
        panelContacts.removeAll();
        constraints.anchor = GridBagConstraints.NORTH;
        for(Reponse c : reponses){
            ReponseView reponseView= new ReponseView(c);
            panelContacts.add(reponseView, constraints);
            constraints.gridy++; 
        }
        validate();
        repaint();
    }

    @Override
    public void receiveReponse(Reponse reponse) {
        try {
            ServiceReponseAPI.getReponses(this,idQuestion);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }


    
}
