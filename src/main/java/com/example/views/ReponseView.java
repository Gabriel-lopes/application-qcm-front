package com.example.views;

import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;

import com.example.models.Question;
import com.example.models.Reponse;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ReponseView extends JPanel implements ActionListener{
    private Reponse reponse;
    private JLabel label;
    private JPanel unPanel = new JPanel();
	private JButton btDeconnexion = new JButton("Déconnexion");


    public ReponseView(Reponse q) {
        super();
        setLayout(new GridBagLayout());
        reponse = q;
		label= new JLabel("");
        JRadioButton radio2 = new JRadioButton(q.getReponse(), false);
		label.setAlignmentX(CENTER_ALIGNMENT);
		add(label);
        add(radio2);
 
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        JOptionPane.showMessageDialog(this, "id " + reponse.getId());

    } 
    
}
