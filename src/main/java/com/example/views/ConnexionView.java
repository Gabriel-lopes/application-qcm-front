package com.example.views;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.*;

import com.example.Services.ServicUtilisateurAPI;
import com.example.models.Utilisateur;

public class ConnexionView extends JFrame implements ActionListener, KeyListener {
    private JPanel unPanel = new JPanel();
	private JButton btAnnuler = new JButton("Annuler");
	private JButton btSeConnecter = new JButton("Se connecter");
	private JTextField txtIdentifant = new JTextField();

    public ConnexionView() {
        this.setTitle("Connexion au QCM");
		this.setBounds(200, 100, 700, 300);
		this.getContentPane().setBackground(new Color(31,62,96,255));
		this.setResizable(false);
		this.setLayout(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Construire le panel :
		this.unPanel.setBounds(350, 50, 300, 180);
		this.unPanel.setLayout(new GridLayout(3,2));
		this.unPanel.setBackground(new Color(31,62,96,255));
		JLabel identifiant = new JLabel("Identifiant : ");
		this.unPanel.add(identifiant);
		this.unPanel.add(txtIdentifant);
		identifiant.setForeground(Color.white);
		this.unPanel.add(this.btAnnuler);
		this.unPanel.add(this.btSeConnecter);

        //Ajout d'une image
		ImageIcon uneImage = new ImageIcon("C:/Users/musta/OneDrive/Bureau/Bac +3/Java/application-qcm-front/src/main/java/com/example/images/photo.png");
		JLabel logo = new JLabel(uneImage);
		logo.setBounds(10, 50, 320, 180);
		this.add(logo);
				
		this.add(this.unPanel); //Ajout du panel dans la fen�tre

        //Rendre les boutons �coutable :
		this.btAnnuler.addActionListener(this);
		this.btSeConnecter.addActionListener(this);
		this.txtIdentifant.addKeyListener(this);
		this.setVisible(true);

		
    }

    @Override
    public void keyTyped(KeyEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.txtIdentifant.getText())
        {
            this.txtIdentifant.setText("");
        }
        else if(e.getSource() == this.btSeConnecter)
        {
            try {
				traitement();
			} catch (NumberFormatException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
        }
        else if(e.getSource() == this.btAnnuler)
        {
            dispose();
        }
    }

    public void traitement() throws NumberFormatException, IOException
	{
		String identifant = this.txtIdentifant.getText();
		try{
			Utilisateur user = ServicUtilisateurAPI.getUtilisateurs(Integer.parseInt(identifant));
			if(user.getId() == Integer.parseInt(identifant))
			{
				JOptionPane.showMessageDialog(this, "Bienvenu " +  user.getPrenom());
				QcmViewApp.getInstance();
				dispose();
			}
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this,"Veuillez vérifier vos identifiant");
			dispose();
		}
	}
    
}
