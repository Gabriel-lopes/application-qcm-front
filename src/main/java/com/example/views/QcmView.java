package com.example.views;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.example.models.Qcm;


public class QcmView extends JPanel implements ActionListener{

	private Qcm qcm;
    private JLabel label;
    private JButton button_question;
    private JPanel unPanel = new JPanel();
	private JButton btDeconnexion = new JButton("Déconnexion");


    public QcmView(Qcm q) {
        super();
        setLayout(new GridBagLayout());
        qcm= q;
		label= new JLabel(q.getId()+" :");
        button_question= new JButton(q.getNom());

		label.setAlignmentX(CENTER_ALIGNMENT);
		button_question.setAlignmentX(CENTER_ALIGNMENT);
        button_question.setSize(30, 20);
        button_question.addActionListener(this);
		add(label);
        add(button_question);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
    	if(e.getSource() == this.button_question)
		{
			QuestionViewApp.getInstance(qcm.getId());
		}
    }
}
