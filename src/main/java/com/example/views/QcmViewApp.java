package com.example.views;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.*;

import com.example.Interfaces.IRequestQcm;
import com.example.Services.ServiceQcmAPI;
import com.example.models.Qcm;
import com.example.Services.ServiceQcmAPI;
public class QcmViewApp extends JFrame implements IRequestQcm, ActionListener{

    GridBagConstraints constraints;
    JPanel panelContacts;
    JButton seDeconnecterBt;

    private static QcmViewApp instance;

    public static QcmViewApp getInstance(){
        if(instance==null){
            instance= new QcmViewApp();
        }
        return instance;
    }

    private QcmViewApp(){
        super("Mes QCMs");
        JLabel label = new JLabel("Voici les QCMs", JLabel.CENTER);
        add(label);
        setVisible(true);
        setSize(600, 400);
        setLayout(new GridBagLayout());
        panelContacts= new JPanel();
        panelContacts.setLayout(new GridBagLayout());
        // Crée un objet de contraintes
        constraints= new GridBagConstraints();
        constraints.anchor = GridBagConstraints.NORTH;
        constraints.gridx = 0; 
        constraints.gridy = 0; 
        constraints.weighty=1;
        add(panelContacts, constraints); 
        constraints.anchor = GridBagConstraints.EAST;
        constraints.gridy = 1;
        constraints.weighty=1;

        seDeconnecterBt = new JButton("Se déconnecter");
        seDeconnecterBt.setAlignmentX(CENTER_ALIGNMENT);
        seDeconnecterBt.addActionListener(this);
        add(seDeconnecterBt, constraints);

        try {
            ServiceQcmAPI.getQcms(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == this.seDeconnecterBt)
        {
            dispose();
        }
    }
    @Override
    public void receiveQcms(ArrayList<Qcm> qcms) {
        constraints.gridx = 0; 
        constraints.gridy = 0;
        panelContacts.removeAll();
        constraints.anchor = GridBagConstraints.NORTH;
        for(Qcm c : qcms){
            QcmView contactView= new QcmView(c);
            panelContacts.add(contactView, constraints);
            constraints.gridy++; 
        }
        validate();
        repaint();
    }

    @Override
    public void receiveQcm(Qcm qcm) {
        try {
            ServiceQcmAPI.getQcms(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
}
