package com.example.views;
import javax.swing.JFrame;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.*;
import com.example.Interfaces.IRequestReponse;
import com.example.Interfaces.IRequestUtilisateur;
import com.example.Services.ServicUtilisateurAPI;
import com.example.Services.ServiceReponseAPI;
import com.example.models.Reponse;
import com.example.models.Utilisateur;

public class UtilisateurViewApp extends JFrame implements IRequestUtilisateur, ActionListener{

    GridBagConstraints constraints;
    JPanel panelContacts;
    JButton seDeconnecterBt;
    JButton envoie_reponse;
    private static UtilisateurViewApp instance;

    public static UtilisateurViewApp getInstance(){
        if(instance==null){
            instance= new UtilisateurViewApp();
        }
        return instance;
    }

    public UtilisateurViewApp(){
        super("Réponses");
    
        setVisible(true);
        setSize(600, 400);
        setLayout(new GridBagLayout());
        panelContacts= new JPanel();
        panelContacts.setLayout(new GridBagLayout());
        // Crée un objet de contraintes
        constraints= new GridBagConstraints();
        constraints.anchor = GridBagConstraints.NORTH;
        constraints.gridx = 0; 
        constraints.gridy = 0; 
        constraints.weighty=1;
        add(panelContacts, constraints); 
        constraints.anchor = GridBagConstraints.EAST;
        constraints.gridy = 1;
        constraints.weighty=1;
        seDeconnecterBt = new JButton("Se connecter");
        seDeconnecterBt.setAlignmentX(CENTER_ALIGNMENT);
        seDeconnecterBt.addActionListener(this);
        add(seDeconnecterBt, constraints);
        try {
            ServicUtilisateurAPI.getUtilisateurs(1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       
        
    }

    @Override
    public void receiveReponses(ArrayList<Utilisateur> utilisateurs) {
        constraints.gridx = 0; 
        constraints.gridy = 0;
        panelContacts.removeAll();
        constraints.anchor = GridBagConstraints.NORTH;
        // for(Utilisateur c : utilisateurs){
        //     ConnexionView reponseView= new ConnexionView();
        //     panelContacts.add(reponseView, constraints);
        //     constraints.gridy++; 
        // }
        validate();
        repaint();
    }
    @Override
    public void receiveReponse(Utilisateur utilisateur) {
        // TODO Auto-generated method stub
        
    }
    
}
