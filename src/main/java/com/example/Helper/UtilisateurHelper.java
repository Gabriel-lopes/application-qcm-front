package com.example.Helper;

import com.example.models.Utilisateur;
import com.google.gson.JsonObject;

public class UtilisateurHelper {
    public static Utilisateur jsonToUtilisateur(JsonObject jsonObject){
        return new Utilisateur(jsonObject.get("id").getAsInt(), 
                        jsonObject.get("prenom").getAsString(),
                        jsonObject.get("nom").getAsString(),
                        jsonObject.get("email").getAsString());
    }
    public static String UtilisateurToJson(Utilisateur utilisateur){
        return "{\"id\":\"" + utilisateur.getId() +"\", ";
    }
}
