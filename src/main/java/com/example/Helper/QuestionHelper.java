package com.example.Helper;

import com.example.models.Question;
import com.google.gson.JsonObject;

public class QuestionHelper {
    public static Question jsonToQuestion(JsonObject jsonObject){
        return new Question(
            jsonObject.get("id").getAsInt(), 
            jsonObject.get("idqcm").getAsInt(), 
            jsonObject.get("question").getAsString());
    }

    public static String questionToJson(Question question){
        return "{\"Question\":\""+question.getQuestion()+"\", ";
    }
}
