package com.example.Helper;

import com.example.models.Qcm;
import com.google.gson.JsonObject;

public class QcmHelper {
    public static Qcm jsonToUtilisateur(JsonObject jsonObject){
        return new Qcm(jsonObject.get("id").getAsInt(), 
                        jsonObject.get("nom").getAsString());
    }

    public static String utilisateurToJson(Qcm qcm){
        return "{\"Nom\":\""+qcm.getNom()+"\", ";
    }
}
