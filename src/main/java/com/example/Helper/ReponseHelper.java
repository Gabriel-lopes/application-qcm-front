package com.example.Helper;

import com.example.models.Reponse;
import com.google.gson.JsonObject;

public class ReponseHelper {
    public static Reponse jsonToReponse(JsonObject jsonObject){
        return new Reponse(jsonObject.get("id").getAsInt(), 
                        jsonObject.get("reponse").getAsString(),
                        jsonObject.get("veracite").getAsBoolean(),
                        jsonObject.get("question").getAsInt());
    }
    public static String reponseToJson(Reponse reponse){
        return "{\"Question\":\"" + reponse.getReponse() +"\", ";

    }
}