package com.example.Services;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import com.example.Helper.UtilisateurHelper;
import com.example.Interfaces.IRequestReponse;
import com.example.Interfaces.IRequestUtilisateur;
import com.example.models.Utilisateur;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ServicUtilisateurAPI {

    public static String API_URL= "http://localhost:8080/api/";

    public static Utilisateur getUtilisateurs(int idUser) throws IOException {
        String requestReponse = requestHTTP("GET", "users/" + idUser);
        JsonObject jsonArray= JsonParser.parseString(requestReponse).getAsJsonObject();
        Utilisateur model = UtilisateurHelper.jsonToUtilisateur(jsonArray);
        return model;
    }

    private static String requestHTTP(String method, String ressource) throws IOException{
        return requestHTTP(method, ressource, null);
    }

    private static String requestHTTP(String method, String ressource, String data) throws IOException{
        URL obj = new URL(API_URL+ressource);
        HttpURLConnection httpURLConnection = (HttpURLConnection) obj.openConnection();
        httpURLConnection.setRequestMethod(method);
        httpURLConnection.setRequestProperty("Content-Type", "application/json");
        if(data!=null){
            httpURLConnection.setDoOutput(true);
            try(OutputStream os = httpURLConnection.getOutputStream()) {
                byte[] input = data.getBytes("utf-8");
                os.write(input, 0, input.length);			
            }
        }
        try(BufferedReader br = new BufferedReader(
            new InputStreamReader(httpURLConnection.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            return response.toString();
        }          
    }
}
